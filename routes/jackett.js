import express from 'express';
import axios from "axios";

import { setEncrypted, getDecrypted } from "../middleware/encryption.js";

const router = express.Router();

const JACKETT_API_KEY = process.env.JACKETT_KEY;
const JACKETT_SERVER_URL = process.env.JACKETT_API
 //   'jackett:9117'
    + "/api/v2.0/indexers/all/results";


import { downloadAndProcess } from "../utils/downloadAndProcess.js";

// Function to extract InfoHash from a magnet link
const extractInfoHashFromMagnet = (magnetLink) => {
  if (!magnetLink)
    return console.log("Invalid magnet link, skipping..."), null;

  const match = magnetLink.match(/btih:([a-zA-Z0-9]+)/);
  if (!match)
    return console.log("Failed to extract InfoHash, skipping..."), null;

  return match[1];
};

router.post("/search", async (req, res) => {
  const { q: searchTerm, limit = 10 } = req.body; // Default limit set to 10

  if (!searchTerm)
    return res.status(400).json({ error: 'Query parameter "q" is required' });

  const cacheKey = `jackett:${searchTerm}`;

  // Try to fetch from cache first
  const cachedResults = await getDecrypted(cacheKey);

  if (cachedResults) return res.json(JSON.parse(cachedResults));

  try {
    const response = await axios.get(JACKETT_SERVER_URL, {
      params: {
        apikey: JACKETT_API_KEY,
        t: "movie",
        Query: searchTerm,
      },
    });

    // Pre-filter and apply the limit as early as possible
    let results = response.data.Results.filter((result) => result.Seeders >= 5)
      .sort((a, b) => b.Seeders - a.Seeders)
      .slice(0, limit);

    // Add magnet links for results missing InfoHash but having a Link
    for (const result of results) {
      if (!result.InfoHash && result.Link) {
        const magnetLink = await downloadAndProcess(result.Link);
        result.MagnetUri = magnetLink;

        // Populate InfoHash from MagnetLink
        result.InfoHash = extractInfoHashFromMagnet(magnetLink);
      }
    }

    // Reduce the result set to only include necessary fields
    const minimalResults = results.map(
      ({
        Title,
        Tracker,
        Details,
        Size,
        Seeders,
        Peers,
        InfoHash,
        MagnetUri,
      }) => ({
        Title,
        Tracker,
        Details,
        Size,
        Seeders,
        Peers,
        InfoHash,
        MagnetUri,
      })
    );

    if (Array.isArray(minimalResults) && minimalResults.length > 0) {
      const resultsToCache = { Results: minimalResults };
       // Encrypt and store the results in Redis with a 1-week expiration
       await setEncrypted(cacheKey, resultsToCache, 604800); // Cache for a week.
      return res.json(resultsToCache);
    } else {
      return res.json({ error: "no results." });
    }
  } catch (error) {
    console.log (`Error fetching data from Jackett API: ${error.message}`);
    return res.status(500).json({ error: "Internal Server Error" });
  }
});


export default router;