import axios from "axios";
import crypto from 'crypto';
import qs from 'qs';

import { setEncrypted, getDecrypted } from "../../middleware/encryption.js";

const API_BASE_URL = process.env.PREMIUMIZE_API;

const axiosInstance = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    "accept": "application/json"
  },
});

async function makePremiumizeRequest(method, url, data, apiKey, authType = 'bearer') {
  try {
    const config = {
      method,
      url,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      }
    };

    // Handle the authorization based on authType
    if (authType === 'bearer') {
      config.headers.Authorization = `Bearer ${apiKey}`;
    } else if (authType === 'apiKey') {
      // Append the apiKey as a query parameter
      config.url += `?apikey=${encodeURIComponent(apiKey)}`;
    }

    // For POST requests, encode data and add to the request body
    if (method.toLowerCase() === "post") {
      config.data = qs.stringify(data);
    } else {
      // For non-POST requests, append data as query parameters
      config.params = data;
    }

    // console.log('Sending request with config:', config);
    const response = await axiosInstance(config);
    return response.data;
  } catch (error) {
    throw error;
  }
}

export async function getTranscodedProxy(hashes, apiKey) {
  // console.log(hashes);
  const magnetLinks = hashes.map((hash) => `magnet:?xt=urn:btih:${hash}`);

  const cacheInfo = await checkCache(magnetLinks, apiKey, true);
  // console.log(cacheInfo);
  if (!cacheInfo) {
    throw new Error("Failed to retrieve cache information for the provided hashes.");
  }

  const directLinks = cacheInfo
    .filter((item) => item.isTranscoded)
    .filter((item) => item.filesize < 10 * 1024 * 1024 * 1024)
    .map((item) => {
      const hash = item.magnetLink.split(":").pop();
      // Concatenate hash and apiKey with a separator
      const combinedString = `${hash}---${apiKey}`;
      const encodedHash = Buffer.from(combinedString).toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=+$/, '');

      const directLink = `${process.env.API_URL}/premiumize/stream/${encodedHash}.mp4`;
      return { magnetLink: item.magnetLink, directLink };
    });

  return directLinks.filter(Boolean);
}

export async function directLink(magnetLink, apiKey, stremio = null) {
  try {
    const hash = magnetLink.match(/btih:([a-zA-Z0-9]+)/)[1];
    // console.log(hash);
    // console.log(magnetLink);
    const cacheKey = `streamLink:${hash}`;

    // Attempt to retrieve and decrypt the stream link from Redis
    let streamLink = await getDecrypted(cacheKey);
    if (streamLink) return streamLink;

    // Determine the API URL
    const apiUrl = stremio ? `https://www.premiumize.me/api/transfer/directdl?apikey=${encodeURIComponent(apiKey)}` 
                            : 'https://www.premiumize.me/api/transfer/directdl';

    // Prepare the request data
    const requestData = qs.stringify({ src: magnetLink });

    // Make a request to the Premiumize API
    const responseData = await axios.post(apiUrl, requestData, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        ...(stremio ? {} : { Authorization: `Bearer ${apiKey}` })
      }
    });

    // console.log('API Response:', responseData.data);

    if (
      responseData.data.status === "success" &&
      responseData.data.content &&
      responseData.data.content.length > 0
    ) {
      streamLink = responseData.data.content.sort((a, b) => b.size - a.size)[0].stream_link;

      // Encrypt and store the stream link in Redis with a 24-hour expiration
      await setEncrypted(cacheKey, streamLink, 86400);
      return streamLink;
    }

    //throw new Error("Unsuccessful API response: " + JSON.stringify(responseData.data));
  } catch (error) {
    //console.error("Error getting direct link:", error);
   // throw error;
  }
}

export async function checkCache(magnetLinks, apiKey, stremio = null) {
  try {
    const firstMagnetLink = magnetLinks[0];

    const cacheKey = crypto.createHash('md5').update(firstMagnetLink).digest('hex');
    const redisCacheKey = `checkCache:${cacheKey}`;

    // Attempt to retrieve and decrypt the data from Redis
    const cachedData = await getDecrypted(redisCacheKey);
    if (cachedData) {
      //console.log('!!! Grabbed from cache !!!');
      return JSON.parse(cachedData);
    }

    // Determine the authType based on the stremio parameter
    const authType = stremio ? "apiKey" : "bearer";

    // If the data is not cached, make the Premiumize request
    const responseData = await makePremiumizeRequest(
      "get",
      "/cache/check",
      { items: magnetLinks },
      apiKey,
      authType
    );

    if (responseData.status === "success") {
      const {
        response: isCached,
        transcoded,
        filename,
        filesize,
      } = responseData;

      // Create a structured result array
      const result = magnetLinks.map((link, index) => ({
        magnetLink: link,
        isCached: isCached[index],
        isTranscoded: transcoded[index],
        filename: filename[index],
        filesize: filesize[index],
      }));

      // Store the result in Redis cache with the hashed key and set expiration to 24 hours (86400 seconds)
      await setEncrypted(redisCacheKey, result, 86400);
      return result;
    }
    // else {
    //   // If the premiumize cache is empty try adding the first magnet link to Premiumize
    //   try {
    //     const result = await addToPremiumize(firstMagnetLink, apiKey, stremio);
    //     console.log("Added to Premiumize:", result);
    //     // Handle successful addition to Premiumize here if needed
    //     return result;
    //   } catch (error) {
    //     console.error("Error adding to Premiumize:", error);
    //     throw error; // Re-throw to be caught by the outer catch block
    //   }
    // }
  } catch (error) {
    console.error("Error checking cache:", error);
    //throw new Error("Failed to check cache", error);
  }
}  
