import express from "express";

import * as premiumize from "./functions.js";

const router = express.Router();

router.post("/getDL", async (req, res) => {
    const { hashes, api_key } = req.body;
    console.log('/getDL called.');
  
    if (!hashes) return res.status(400).json({ error: "hashes are required" });
  
    try {
      const result = await premiumize.getTranscodedProxy(
        hashes,
        api_key
      );
  
      if (!result) return res.status(500).json({ error: "Error processing the request" });
  
      return res.status(200).json({ data: result });
    } catch (error) {
      return res.status(500).json({ error: "Internal Server Error" });
    }
  });
  
  router.get('/stream/:hash.mp4', async (req, res) => {
    const encodedHash = req.params.hash;
  
    // Decode the URL-safe Base64 hash and split to get the magnet hash and apiKey
    const decodedString = Buffer.from(encodedHash
      .replace(/-/g, '+')
      .replace(/_/g, '/'),
      'base64')
      .toString('utf-8');
    
    const [hash, apiKey] = decodedString.split('---');
    const magnetLink = 'magnet:?xt=urn:btih:' + hash;
  
    if (!magnetLink || !apiKey)
      return res.status(400).send('Missing magnetLink or apiKey');
  
    try {
      const directLinkUrl = await premiumize.directLink(magnetLink, apiKey, true);
      if (!directLinkUrl) return res.status(404).send('No direct link found');
      return res.redirect(directLinkUrl);
    } catch (error) {
      console.error('Error in /stream:', error);
      res.status(500).send('Internal Server Error');
    }
  });
  
  export default router;