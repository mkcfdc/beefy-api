import express from "express";

import morgan from "morgan";
//import cors from "cors";
import compression from "compression";

// Routes:
import jackettRoutes from './routes/jackett.js';
import premiumizeRoutes from './routes/premiumize/routes.js';

const PORT = process.env.PORT || 3030;

// Promise problems????
process.on('unhandledRejection', (reason, promise) => {
  console.error('Unhandled Promise Rejection:', reason);
  // Handle or log the rejection reason here
});

// Initialize app
const app = express();


// Turn on logging based on enviorment;
process.env.NODE_ENV === "dev" && app.use(morgan("dev"));

//app.use(cors());
app.use(express.json());
app.use(compression());

// Use the routes with your Express application
app.use('/jackett', jackettRoutes);
app.use('/premiumize', premiumizeRoutes);

app.listen(PORT, () =>
  console.log(`Server is running on http://localhost:${PORT}`)
);