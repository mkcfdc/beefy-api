import { randomBytes, createCipheriv, createDecipheriv } from 'crypto';
import { client } from './redisClient.js';

// Encryption and Decryption configuration
const algorithm = 'aes-256-cbc';
const key = process.env.ENCRYPTION_KEY;

// Encrypt function
function encrypt(text) {
  const iv = randomBytes(16); // Initialization vector
  const keyBytes = Buffer.from(key, 'hex'); // Convert hex string to bytes
  let cipher = createCipheriv(algorithm, keyBytes, iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

// Decrypt function
function decrypt(text) {
  let iv = Buffer.from(text.iv, 'hex');
  const keyBytes = Buffer.from(key, 'hex'); // Convert hex string to bytes
  let encryptedText = Buffer.from(text.encryptedData, 'hex');
  let decipher = createDecipheriv(algorithm, keyBytes, iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

export async function setEncrypted(key, value, expireTime) {
  try {
    // console.log(`Encrypting data for key: ${key}`);

    // Serialize the value to a JSON string if it's not a string
    const valueToEncrypt = typeof value === 'string' ? value : JSON.stringify(value);

    const encryptedValue = encrypt(valueToEncrypt);
    // console.log(`Data encrypted. Saving to Redis with key: ${key} and expiration time: ${expireTime} seconds`);

    await client.set(key, JSON.stringify(encryptedValue), 'EX', expireTime);
    // console.log(`Data successfully saved to Redis with key: ${key}`);

    return true;
  } catch (error) {
    console.error(`Error in setEncrypted for key: ${key}`, error);
    throw error; // or handle it as per your application's error handling strategy
  }
}

export async function getDecrypted(key) {
  try {
    const encryptedValue = await client.get(key);

    if (!encryptedValue) {
      return null; // or handle the absence of the key as needed
    }

    const decryptedValue = decrypt(JSON.parse(encryptedValue));

    // Parse the decrypted value back into a JSON object
    return decryptedValue;
  } catch (error) {
    console.error(`Error in getDecrypted for key: ${key}`, error);
    throw error; // or handle it as per your application's error handling strategy
  }
}
